Curso de Udemy "LA BIBLIA PERDIDA DE BOOTSTRAP 4" por Erick Mines
Duración: 9,3 horas

========================================HERRAMIENTAS===============================================================
Preprocesadores SCSS
+ http://koala-app.com/
+ https://prepros.io/

* Crear tu propio favicon: http://tools.dynamicdrive.com/favicon/

Cargar automaticamente el navegador cuando se haga un cambio en Chrome:
Livepage:
https://mikerogers.io/ -> https://mikerogers.io/portfolio.html -> https://livepage.mikerogers.io/
-> https://github.com/MikeRogers0/LivePage

Instalación
1- Descargar y descomprimir
2- Ir a Google Chrome -> : -> más herramientas -> Extensiones ->Modo de desarrollador activo -> Cargar descomprimdia (y se elige el archivo que se descargo ya descomprimido)
3- Una vez cargado ir a detalles -> Permitir en modo de incógnito aparte de que las otras opciones esten activas

Bootstrap4 Instalación:
CDN: https://getbootstrap.com/docs/4.4/getting-started/introduction/
Chuleta con clases de bootstrap 4: https://hackerthemes.com/bootstrap-cheatsheet/
Plugin sublime text: Preferences-> package control -> Install package -> Bootstrap 4 Autocomplete


=======================================================NOTAS=======================================================
Archivos Bootstrap:
+ bootstrap.css: contiene todos los estilos CSS que Bootstrap nos regala a través de sus clases para poder crear el sistema de columnas, efectos para imágenes, ventanas modales,etc.
+ bootstrap.min.css: normalmente el archivo anterior no se usa; se usa este ya que es la versión comprimida del anterior.
+ bootstrap.css.map: archivos de rastreo que usan los compiladores SASS o LESS, así que se omiten los archivos .map
+ bootstrap-grid.css: para proyectos donde no interesa usar los componentes de bootstrap, como para los estilos de los botones que nos ofrece bootsratp. Solamente interesa usar el sistema de columnas.
+ bootstrap-reboot.css: sirve para reinicializar los estilos de todas las etiquetas HTML porque normalmente los navegadores asignan diferentes estilos a las etiquetas HTML. Algunos por defecto el texto es de 15px, otros de 16px. Algunos a los encabezados le asignan un margin de 14,15,13 px y esto hace que las etiquetas HTML no se muestren idénticas en los diferentes navegadores. Para quitar esto se aplica el reboot.

https://jquery.com/download/

Componentes que requieren JavaScript: 
* Alertas por despido
* Botones para alternar estados y casilla de verificación / funcionalidad de radio
* Carrusel para todos los comportamientos de diapositivas, controles e indicadores.
* Contraer para alternar la visibilidad del contenido
* Desplegables para mostrar y posicionar (también requiere Popper.js )
* Modalidades de visualización, posicionamiento y comportamiento de desplazamiento.
* Barra de navegación para extender nuestro complemento Collapse para implementar un comportamiento receptivo
* Información sobre herramientas y elementos emergentes para mostrar y posicionar (también requiere Popper.js )
* Scrollspy para el comportamiento del desplazamiento y las actualizaciones de navegación

DOCE REGLAS MAS UNA EXTRA DEL SISTEMA DE COLUMNAS:
1. 3 son los elementos que conforman este sistema: un contenedor, las filas y columnas
2. Tenemos dos tipos de contenedores: container (widht:80-90% anchura del navegador) y container-fluid (100% anchura del navegador)
3. Las filas tienen que ser hijos directos de un contenedor (etiqueta que esta dentro de otra)
4. Las columnas deben de ser hijos directos de una fila. EL contenido se crea dentro d elas columnas y no dentro de las filas
<div class="container">
	<div class="row">
		<div class="col-md-12">
			Texto
		</div>
	</div>
</div>
5. Podemos tener como máximo 12 columnas dentro de una sola fila
6. Si superamos las doce columnas por fila, las columnas restantes saltaran abajo formando una nueva fila
7. El contenido tiene que ir dentro de las columnas
8. La clase "col" nos permite crear columnas de proporciones idénticas
9. Podemos modificar la medida de una columna, dependiendo del tamaño de pantalla.
Bootstrap maneja 5 contextos: col-* (por defecto), col-sm-*, col-md-*, col-lg-* y col -xl-*
10. Los contextos se heredan de menor a mayor
11. Podemos crear columnas vacías. Usando offset-* podemos mover una cantidad de columnas hacia la derecha. offset-sm-2
12. Podemos crear columnas anidadas
13. Podemos reordenar las columnas. order-first

MAS NOTAS:
+ REM es una medida relativa basada en el tamaño de fuente que tenga la etiqueta HTML. Si la etiqueta HTML tiene definido un tamaño de fuente de 16px, entonces 1rem=16px; si es 14px entonces 1 rem=14px  y así.
+ Los parráfos pueden tomar la apariencia de encabezados. <p class="h3">Soy un parráfo con tamaño h3 de encabezado</p> Clases para encabezados: de h1 a h6
+ Pueden tomar también tamaños de títulos destacados. <p class="display-1">Supertitulo o titulo destacado con texto de mayor tamaño</p> display-1 al display-4
+ display: none; Esconde el elemento
+ display:block; se muestran los elementos uno debajo del otro ya que ocupan el 100% del ancho que tengan
+ display:inline; permite cambiar el comportamiento a una etiqueta para que sea en línea. Su anchura depende solo del contenido. no se puede poner anchura ni altura
+ display: inline-block; permite crear elementos en linea pero que soportan la propiedad width y height para aplicar una anchura y una altura
+ display: flex; crea un entorno flexible para manipular a sus hijos pero con propiedades como display.block

